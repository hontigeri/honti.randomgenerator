﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honti.RandomGenerator
{
    public partial class Generator
    {
        public static DateTime GenerateDateTime()
        {
            var rnd = new Random();
            return DateTime.Now.AddDays(rnd.Next()).AddHours(rnd.Next()).AddMilliseconds(rnd.Next()).AddMonths(rnd.Next());
        }

        public static DateTime GenerateDateTime(DateTime lowestClaim, DateTime highestClaim)
        {
            var rnd = new Random();
            var timespan = highestClaim - lowestClaim;
            var hourspan = (int)Math.Round(timespan.TotalHours);
            return DateTime.Now.AddHours(rnd.Next(hourspan));
        }
    }
}
