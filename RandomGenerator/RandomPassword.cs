﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Honti.RandomGenerator
{

    public partial class Generator
    {
        private const string PasswordCharsLcase = "abcdefgijkmnopqrstwxyz";
        private const string PasswordCharsUcase = "ABCDEFGHJKLMNPQRSTWXYZ";
        private const string PasswordCharsNumeric = "23456789";
        private const string PasswordCharsSpecial = "*$-+?_&=!%{}/";


        public static string GeneratePassword(int minLength, int maxLength)
        {
            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;

            var charGroups = new[]
                {
                    PasswordCharsLcase.ToCharArray(),
                    PasswordCharsUcase.ToCharArray(),
                    PasswordCharsNumeric.ToCharArray(),
                    PasswordCharsSpecial.ToCharArray()
                };


            var charsLeftInGroup = new int[charGroups.Length];


            for (var loop = 0; loop < charsLeftInGroup.Length; loop++)
                charsLeftInGroup[loop] = charGroups[loop].Length;


            var leftGroupsOrder = new int[charGroups.Length];


            for (var loop = 0; loop < leftGroupsOrder.Length; loop++)
                leftGroupsOrder[loop] = loop;


            var randomBytes = new byte[4];


            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);


            var seed = (randomBytes[0] & 0x7f) << 24 |
                       randomBytes[1] << 16 |
                       randomBytes[2] << 8 |
                       randomBytes[3];


            var random = new Random(seed);


            var password = minLength < maxLength
                ? new char[random.Next(minLength, maxLength + 1)]
                : new char[minLength];


            var lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;


            for (var loop = 0; loop < password.Length; loop++)
            {
                int nextLeftGroupsOrderIdx;
                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0,
                        lastLeftGroupsOrderIdx);


                var nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];


                var lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                var nextCharIdx = lastCharIdx == 0 ? 0 : random.Next(0, lastCharIdx + 1);


                password[loop] = charGroups[nextGroupIdx][nextCharIdx];


                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                        charGroups[nextGroupIdx].Length;

                else
                {
                    if (lastCharIdx != nextCharIdx)
                    {
                        var temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                            charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }

                    charsLeftInGroup[nextGroupIdx]--;
                }


                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

                else
                {
                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        var temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                            leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }

                    lastLeftGroupsOrderIdx--;
                }
            }


            return new string(password);
        }
    }
}