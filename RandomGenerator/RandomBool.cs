﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honti.RandomGenerator
{
    public partial class Generator
    {
        public static bool GenerateBool()
        {
            var rnd = new Random();
            return rnd.Next() % 2 == 1;
        }
    }
}
